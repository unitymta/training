import React, { Component } from 'react';
import Header from './Component/Header';
import Nav from './Component/Nav';
import Banner from './Component/Home/Banner';
import Category from './Component/Home/Category';
import Tags from './Component/Home/Tags';
import NewProduct from './Component/Home/NewProduct';
import BestSeller from './Component/Home/BestSeller';
import RecentlyView from './Component/Home/RecentlyView';
import Brand from './Component/Home/Brand';
import Footer from './Component/Footer';

import './App.css';

class App extends Component {
	render() {
		return (
			<div id="smv-wrapper">
				<Header />
				<Nav />
				<Banner />
				<Category />
				<Tags />
				<div className="smv-block-content" id="smv-container">
					<div className="smv-wrap">
						<NewProduct />
						<BestSeller />
						<RecentlyView/>
					</div>
				</div>
				<Brand/>
				<Footer/>
				<div className="smv-overlay-bg" />
			</div>
		);
	}
}

export default App;
