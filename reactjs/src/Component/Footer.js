import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return (
            <div id="smv-main-footer">
                <div className="smv-block-content smv-newsletter">
                    <div className="smv-wrap">
                        <div className="smv-title-newsletter">
                            <h3 className="title">Sign up for our Newsletter <span>to stay up-to-date on our promotions, special offers and more...</span></h3>
                        </div>
                        <div className="smv-form-newsletter">
                            <form action method="get">
                                <input name="email" placeholder="Email here..." type="email" />
                                <button>Go</button>
                            </form>
                        </div>
                        <div className="smv-follow">
                            <h5>Follow Us</h5>
                            <ul>
                                <li className="smv-face-icon"><a title href="#"><i className="fa fa-facebook" /></a></li>
                                <li className="smv-instagram-icon"><a title href="#"><i className="fa fa-instagram" /></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <footer id="smv-footer">
                    <div className="smv-wrap">
                        <div className="smv-cate-item smv-category-footer">
                            <div className="smv-wrap-item">
                                <h3 className="smv-item-title">Shop by Category</h3>
                                <ul>
                                    <li><a href="#" title="4X4">4X4</a></li>
                                    <li><a href="#" title="Camping">Camping</a></li>
                                    <li><a href="#" title="Packaging">Packaging</a></li>
                                    <li><a href="#" title="Accessories">Accessories</a></li>
                                    <li><a href="#" title="Electrical">Electrical</a></li>
                                    <li><a href="#" title="Electronics">Electronics</a></li>
                                    <li><a href="#" title="Electrical">Electrical</a></li>
                                    <li><a href="#" title="Home & Garden">Home &amp; Garden</a></li>
                                    <li><a href="#" title="Home">Home</a></li>
                                    <li><a href="#" title="Marine">Marine</a></li>
                                    <li><a href="#" title="Tools">Tools</a></li>
                                    <li><a href="#" title="New Arrivals">New Arrivals</a></li>
                                    <li><a href="#" title="Replacement Parts">Replacement Parts</a></li>
                                </ul>
                            </div>
                        </div>
                        <div className="smv-cate-item smv-help">
                            <div className="smv-wrap-item">
                                <h3 className="smv-item-title">Help &amp; Advice</h3>
                                <ul>
                                    <li><a title="About Us" href="#" target="_self">About Us</a></li>
                                    <li><a title="payment" href="#">Payment</a></li>
                                    <li><a title="Delivery" href="#">Delivery</a></li>
                                    <li><a title="Returns" href="#">Returns</a></li>
                                    <li><a title="Requests & Sourcing" href="#">Requests &amp; Sourcing</a></li>
                                    <li><a title="Contact Us" href="#">Contact Us</a></li>
                                </ul>
                            </div>
                        </div>
                        <div className="smv-cate-item smv-account">
                            <div className="smv-wrap-item">
                                <h3 className="smv-item-title">My Account</h3>
                                <p className="smv-group-login"><a href="#" title="LOGIN" className="smv-login">LOGIN</a><a href="#" title="SIGN UP">SIGN UP</a></p>
                                <ul>
                                    <li><a href="#" title="My Account">My Account</a></li>
                                    <li><a href="#" title="Order History">Order History</a></li>
                                    <li><a href="#" title="Change Password">Change Password</a></li>
                                </ul>
                            </div>
                        </div>
                        <div className="smv-cate-item smv-accept">
                            <div className="smv-wrap-item">
                                <h3 className="smv-item-title">We ACCEPT</h3>
                                <ul>
                                    <li><a title href="#"><img src="./images/static/MH-accept-1.png" alt='x' /></a></li>
                                    <li><a title href="#"><img src="./images/static/MH-accept-2.png" alt='x' /></a></li>
                                    <li><a title href="#"><img src="./images/static/MH-accept-3.png" alt='x' /></a></li>
                                    <li><a title href="#"><img src="./images/static/MH-accept-4.png" alt='x' /></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="smv-copy-right">
                        <div className="smv-wrap">
                            <div className="smv-footer-copy-left">
                                <a title="Machine Head" href="#">
                                    <img title="Machine Head" src="./images/logo.png" alt="Machine Head" />
                                </a>
                            </div>
                            <div className="smv-footer-copy-right">
                                <p>
                                    <span>Copyright 2016 Machine Head International Pty </span>
                                    <span>ACN: 609 523 819 | ABN: 74 958 585 726</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        );
    }
}

export default Footer;