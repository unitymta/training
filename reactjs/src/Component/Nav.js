import React, { Component } from 'react';

class Nav extends Component {
    render() {
        return (
            <div className="smv-block-content smv-category">
                <div className="smv-wrap">
                    <ul className="smv-nav">
                        <li className="smv-title-menu"><a href="#" title="home">Home</a></li>
                        <li>
                            <a href="#" title="4X4">4X4</a>
                            <i className="arrow fa fa-angle-right" aria-hidden="true" />
                            <i className="arrow fa fa-angle-down" aria-hidden="true" />
                            <div className="sub-category">
                                <ul>
                                    <li><a href="#" title>Submenu 1</a></li>
                                    <li>
                                        <a href="#" title>Submenu 1</a>
                                        <i className="arrow fa fa-angle-right" aria-hidden="true" />
                                        <i className="arrow fa fa-angle-down" aria-hidden="true" />
                                        <div className="sub-category2">
                                            <ul>
                                                <li><a href="#" title>Submenu 2</a></li>
                                                <li><a href="#" title>Submenu 2</a></li>
                                                <li><a href="#" title>Submenu 2</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li><a href="#" title>Submenu 1</a></li>
                                </ul>
                            </div>
                        </li>
                        <li><a href="#" title="Accessories">Accessories</a></li>
                        <li><a href="#" title="Electrical">Electrical</a></li>
                        <li><a href="#" title="Replacement Parts">Replacement Parts</a>
                            <i className="arrow fa fa-angle-right" aria-hidden="true" />
                            <i className="arrow fa fa-angle-down" aria-hidden="true" />
                            <div className="sub-category">
                                <ul>
                                    <li><a href="#" title>Submenu 1</a></li>
                                    <li>
                                        <a href="#" title>Submenu 1</a>
                                        <i className="arrow fa fa-angle-right" aria-hidden="true" />
                                        <i className="arrow fa fa-angle-down" aria-hidden="true" />
                                        <div className="sub-category2">
                                            <ul>
                                                <li><a href="#" title>Submenu 2</a></li>
                                                <li><a href="#" title>Submenu 2</a></li>
                                                <li><a href="#" title>Submenu 2</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li><a href="#" title>Submenu 1</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}

export default Nav;