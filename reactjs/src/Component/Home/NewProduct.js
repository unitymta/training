import React, { Component } from 'react';

class NewProduct extends Component {
    render() {
        return (
            <div className="smv-block-content" id="smv-container">
                <div className="smv-wrap">
                    <div className="smv-clr smv-new-arrivals">
                        <div className="smv-title-block">
                            <h2>New Arrivals</h2>
                            <div className="smv-btn-view-all"><a href="#" title="View all">View all<i className="fa fa-caret-right" aria-hidden="true" /></a></div>
                        </div>
                        <div className="smv-new-arrivals-slider smv-clr">
                            <ul className="smv-product-list">
                                <li className="smv-product-item">
                                    <div className="smv-item">
                                        <div className="smv-item-img">
                                            <a href="#" title><span><img src="./images/static/product-best-seller-1.jpg" alt='x' /></span></a>
                                            <div className="smv-sale-off">
                                                <span>
                                                    <i className="smv-sale-number">50%</i>
                                                    <i>Off</i>
                                                </span>
                                            </div>
                                            <p className="smv-shipping-method">Free shipping</p>
                                        </div>
                                        <div className="smv-item-info">
                                            <h2 className="smv-product-name"><a href="#" title>Roadsafe Tow Points Toyota Land-cruiser 70 Series - extended length</a></h2>
                                            <div className="smv-more-info">
                                                <div className="smv-reviews">
                                                    <span className="smv-stars smv-stars-100" /><span className="smv-count">(5)</span>
                                                </div>
                                            </div>
                                            <div className="smv-item-price">
                                                <p className="smv-new-price">$144.00</p>
                                                <p className="smv-old-price">was $198.00</p>
                                                <p className="smv-mrsp-price">mrsp $140.00</p>
                                                <p className="smv-save-price">save 20%</p>
                                            </div>
                                            <span className="smv-item-condition">As low as $50.00</span>
                                            <div className="smv-btn-add smv-add-cart">
                                                <a href="#"><span className="smv-text">add to cart</span><i className="fa fa-shopping-cart" aria-hidden="true" /></a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li className="smv-product-item">
                                    <div className="smv-item">
                                        <div className="smv-item-img">
                                            <a href="#" title><span><img src="./images/static/product-best-seller-2.jpg" alt='x' /></span></a>
                                        </div>
                                        <div className="smv-item-info">
                                            <h2 className="smv-product-name"><a href="#" title>Roadsafe Extended Shackles Ford PX Ranger / Mazda BT50 (Gen2)</a></h2>
                                            <div className="smv-more-info">
                                                <div className="smv-reviews">
                                                    <span className="smv-stars smv-stars-100" /><span className="smv-count">(5)</span>
                                                </div>
                                            </div>
                                            <div className="smv-item-price">
                                                <p className="smv-new-price">$180.00</p>
                                            </div>
                                            <span className="smv-item-condition">As low as $50.00</span>
                                            <div className="smv-btn-add smv-coming-soon">
                                                <a href="#"><span className="smv-text">Stock Coming Soon</span><i className="fa fa-shopping-cart" aria-hidden="true" /></a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li className="smv-product-item">
                                    <div className="smv-item">
                                        <div className="smv-item-img">
                                            <a href="#" title><span><img src="./images/static/product-best-seller-3.jpg" alt='x' /></span></a>
                                        </div>
                                        <div className="smv-item-info">
                                            <h2 className="smv-product-name"><a href="#" title>Ford Transit Van, VM, By Pass Oil Filter, 2.4L Engine Oil Filter Cartridge, 09/2006-02/2014</a></h2>
                                            <div className="smv-more-info">
                                                <div className="smv-reviews">
                                                    <span className="smv-stars smv-stars-100" /><span className="smv-count">(5)</span>
                                                </div>
                                            </div>
                                            <div className="smv-item-price">
                                                <p className="smv-new-price">$27.00</p>
                                            </div>
                                            <span className="smv-item-condition">As low as $50.00</span>
                                            <div className="smv-btn-add smv-add-cart">
                                                <a href="#"><span className="smv-text">add to cart</span><i className="fa fa-shopping-cart" aria-hidden="true" /></a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li className="smv-product-item">
                                    <div className="smv-item">
                                        <div className="smv-item-img">
                                            <a href="#" title><span><img src="./images/static/product-best-seller-4.jpg" alt='x' /></span></a>
                                            <div className="smv-sale-off">
                                                <span>
                                                    <i className="smv-sale-number">50%</i>
                                                    <i>Off</i>
                                                </span>
                                            </div>
                                            <p className="smv-shipping-method">Free shipping</p>
                                        </div>
                                        <div className="smv-item-info">
                                            <h2 className="smv-product-name"><a href="#" title> VW Volkswagen Crafter, Mercedes Sprinter , Indicator Lense, for RH Mirror, 10/2006&gt;</a></h2>
                                            <div className="smv-more-info">
                                                <div className="smv-reviews">
                                                    <span className="smv-stars smv-stars-100" /><span className="smv-count">(5)</span>
                                                </div>
                                            </div>
                                            <div className="smv-item-price">
                                                <p className="smv-new-price">$144.00</p>
                                                <p className="smv-old-price">was $198.00</p>
                                                <p className="smv-mrsp-price">mrsp $140.00</p>
                                                <p className="smv-save-price">save 20%</p>
                                            </div>
                                            <span className="smv-item-condition">As low as $50.00</span>
                                            <div className="smv-btn-add smv-add-cart">
                                                <a href="#"><span className="smv-text">add to cart</span><i className="fa fa-shopping-cart" aria-hidden="true" /></a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default NewProduct;