import React, { Component } from 'react';

class Category extends Component {
    render() {
        return (
            <div className="smv-block-content smv-featured-categories">
                <div className="smv-wrap">
                    <div className="smv-title-block">
                        <h2>Featured Categories</h2>
                    </div>
                    <div className="smv-wrap-cate">
                        <div className="smv-cate-item">
                            <div className="smv-wrap-item">
                                <div className="smv-cate-img">
                                    <img src="./images/static/category-1.jpg" alt='x' />
                                </div>
                                <div className="smv-cate-name">
                                    <h3>4WD</h3>
                                </div>
                                <a className="smv-cursor-arrow">
                                </a>
                                <a href="#" className="smv-overlay" />
                            </div>
                        </div>
                        <div className="smv-cate-item">
                            <div className="smv-wrap-item">
                                <div className="smv-cate-img">
                                    <img src="./images/static/category-2.jpg" alt='x' />
                                </div>
                                <div className="smv-cate-name">
                                    <h3>Accessories</h3>
                                </div>
                                <a className="smv-cursor-arrow">
                                </a>
                                <a href="#" className="smv-overlay" />
                            </div>
                        </div>
                        <div className="smv-cate-item">
                            <div className="smv-wrap-item">
                                <div className="smv-cate-img">
                                    <img src="./images/static/category-3.jpg" alt='x' />
                                </div>
                                <div className="smv-cate-name">
                                    <h3>Electrical Products</h3>
                                </div>
                                <a className="smv-cursor-arrow">
                                </a>
                                <a href="#" className="smv-overlay" />
                            </div>
                        </div>
                        <div className="smv-cate-item">
                            <div className="smv-wrap-item">
                                <div className="smv-cate-img">
                                    <img src="./images/static/category-4.jpg" alt='x' />
                                </div>
                                <div className="smv-cate-name">
                                    <h3>Replacement Parts</h3>
                                </div>
                                <a className="smv-cursor-arrow">
                                </a>
                                <a href="#" className="smv-overlay" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Category;