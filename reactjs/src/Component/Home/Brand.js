import React, { Component } from 'react';

class Brand extends Component {
    render() {
        return (
            <div className="smv-block-content smv-our-brands">
                <div className="smv-wrap">
                    <div className="smv-title-block">
                        <h2>Our Trusted Brands</h2>
                        <div className="smv-btn-view-all"><a href="#" title="View All Brands">View All Brands<i className="fa fa-caret-right" aria-hidden="true" /></a></div>
                    </div>
                    <div className="smv-brands-content">
                        <ul className="smv-brands-list">
                            <li className="smv-brand-item"><a href="#" title><img src="./images/static/MH-icon-brand-1.png" alt='x' /></a></li>
                            <li className="smv-brand-item"><a href="#" title><img src="./images/static/MH-icon-brand-2.png" alt='x' /></a></li>
                            <li className="smv-brand-item"><a href="#" title><img src="./images/static/MH-icon-brand-3.png" alt='x' /></a></li>
                            <li className="smv-brand-item"><a href="#" title><img src="./images/static/MH-icon-brand-4.png" alt='x' /></a></li>
                            <li className="smv-brand-item"><a href="#" title><img src="./images/static/MH-icon-brand-5.png" alt='x' /></a></li>
                            <li className="smv-brand-item"><a href="#" title><img src="./images/static/MH-icon-brand-6.png" alt='x' /></a></li>
                            <li className="smv-brand-item"><a href="#" title><img src="./images/static/MH-icon-brand-7.png" alt='x' /></a></li>
                            <li className="smv-brand-item"><a href="#" title><img src="./images/static/MH-icon-brand-8.png" alt='x' /></a></li>
                            <li className="smv-brand-item"><a href="#" title><img src="./images/static/MH-icon-brand-9.png" alt='x' /></a></li>
                            <li className="smv-brand-item"><a href="#" title><img src="./images/static/MH-icon-brand-10.png" alt='x' /></a></li>
                            <li className="smv-brand-item"><a href="#" title><img src="./images/static/MH-icon-brand-11.png" alt='x' /></a></li>
                            <li className="smv-brand-item"><a href="#" title><img src="./images/static/MH-icon-brand-12.png" alt='x' /></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

export default Brand;