import React, { Component } from 'react';

class Banner extends Component {
    render() {
        return (
            <div className="smv-block-content smv-block-banner">
                <div className="smv-slide-banner">
                    <ul className="smv-slider">
                        <li>
                            <a href="#"><img src="./images/static/banner-1.jpg" alt='x' /></a>
                        </li>
                        <li>
                            <a href="#"><img src="./images/static/banner-2.jpg" alt='x' /></a>
                        </li>
                        <li>
                            <a href="#"><img src="./images/static/banner-3.jpg" alt='x' /></a>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}

export default Banner;