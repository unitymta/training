import React, { Component } from 'react';

class Tags extends Component {
    render() {
        return (
            <div className="smv-block-content smv-tags">
                <div className="smv-wrap">
                    <div className="smv-wrap-tags">
                        <div className="smv-tags-item">
                            <div className="smv-wrap-item">
                                <p>Australia <span>Delivery</span></p>
                                <p><img src="./images/static/MH-icon-tag-australia.png" alt='x' /></p>
                            </div>
                        </div>
                        <div className="smv-tags-item">
                            <div className="smv-wrap-item">
                                <p>100% Satisfaction  <span>Guarantee</span></p>
                                <p><img src="./images/static/MH-icon-tag-satisfaction.png" alt='x' /></p>
                            </div>
                        </div>
                        <div className="smv-tags-item">
                            <div className="smv-wrap-item">
                                <p>Genuine <span>Genuine Warranty</span></p>
                                <p><img src="./images/static/MH-icon-tag-genuine.png" alt='x' /></p>
                            </div>
                        </div>
                        <div className="smv-tags-item">
                            <div className="smv-wrap-item">
                                <p>Local <span>Support</span></p>
                                <p><img src="./images/static/MH-icon-tag-local.png" alt='x' /></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Tags;