import React, { Component } from 'react';

class RecentlyView extends Component {
    render() {
        return (
            <div className="smv-clr smv-recently-viewed">
                <div className="smv-title-block">
                    <h2>Recently Viewed</h2>
                </div>
                <ul className="smv-list-recently smv-clr">
                    <li>
                        <div className="smv-images">
                            <a href="#"><img src="./images/static/MH-img-recently-view-1.png" alt='x' /></a>
                        </div>
                        <div className="smv-text">
                            <p><a href="#">Spyder U-Bar Style Projector Headlights</a></p>
                        </div>
                    </li>
                    <li>
                        <div className="smv-images">
                            <a href="#"><img src="./images/static/MH-img-recently-view-2.png" alt='x' /></a>
                        </div>
                        <div className="smv-text">
                            <p><a href="#">Spyder U-Bar Style Projector Headlights</a></p>
                        </div>
                    </li>
                    <li>
                        <div className="smv-images">
                            <a href="#"><img src="./images/static/MH-img-recently-view-1.png" alt='x' /></a>
                        </div>
                        <div className="smv-text">
                            <p><a href="#">Spyder U-Bar Style Projector Headlights</a></p>
                        </div>
                    </li>
                    <li>
                        <div className="smv-images">
                            <a href="#"><img src="./images/static/MH-img-recently-view-2.png" alt='x' /></a>
                        </div>
                        <div className="smv-text">
                            <p><a href="#">Spyder U-Bar Style Projector Headlights</a></p>
                        </div>
                    </li>
                    <li>
                        <div className="smv-images">
                            <a href="#"><img src="./images/static/MH-img-recently-view-1.png" alt='x' /></a>
                        </div>
                        <div className="smv-text">
                            <p><a href="#">Spyder U-Bar Style Projector Headlights</a></p>
                        </div>
                    </li>
                    <li>
                        <div className="smv-images">
                            <a href="#"><img src="./images/static/MH-img-recently-view-2.png" alt='x' /></a>
                        </div>
                        <div className="smv-text">
                            <p><a href="#">Spyder U-Bar Style Projector Headlights</a></p>
                        </div>
                    </li>
                </ul>
            </div>
        );
    }
}

export default RecentlyView;