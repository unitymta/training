import React, { Component } from 'react';

class Header extends Component {
    render() {
        return (
            <div id="smv-main-header">
                <div className="smv-top-header">
                    <div className="smv-wrap smv-clr">
                        <div className="smv-nav-bar smv-hide-des">
                            <a href="#" className="smv-icon smv-icon-bar"><i className="fa fa-bars" aria-hidden="true" /></a>
                        </div>
                        <div className="smv-head-left smv-logo">
                            <a href="#"><img src="./images/logo.png" alt='x' /></a>
                        </div>
                        <div className="smv-head-right smv-right">
                            <ul className="smv-nav smv-clr">
                                <li className="smv-blog"><a href="#">Blog</a></li>
                                <li className="smv-login"><a href="#">Login or Sign up</a></li>
                                <li className="smv-wishlist"><a href="#">Wishlist</a></li>
                            </ul>
                            <div className="smv-mini-cart">
                                <a href="#">
                                    <span className="smv-qty-item">0</span>
                                    <span className="smv-toal"> </span>
                                </a>
                                <div className="smv-wrap-mini-cart">
                                    <p className="smv-emptry">You have no items in your shopping cart.</p>
                                </div>
                            </div>
                        </div>
                        <div className="smv-head-right-bottom smv-right">
                            <div className="smv-call-now smv-right">
                                <p>Need help? Call us now</p>
                                <p><a href="tel:03 - 8770 2211" title>03 - 8770 2211</a></p>
                            </div>
                            <div className="smv-search-box smv-right">
                                <div className="smv-input-box">
                                    <form action method="get">
                                        <input type="text" name="search" placeholder="Search entire store here..." />
                                        <button><i className="fa fa-search" aria-hidden="true" /></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Header;