var sellSlider;
if (typeof require != "undefined" && typeof define != "undefined") {
	require([
	    'jquery',
	    'owlCarousel'
	], function($,owlCarousel){
			var sprF = varFunctions($,owlCarousel);
			$(document).ready(function(){
				sprF.initfunction();
				sprF.initSingleSlider();
				sprF.initSellSlider('.smv-best-sellers');
				sprF.initSellSlider('.smv-new-arrivals');
				sprF.initBrandSlider();
				sprF.handleToChangeTabContent();
				sprF.collapseNavBar();
				sprF.expandFilter();
			});
			$(window).load(function(){
				
			});
			$(window).on("resize",function(){
				sprF.initBrandSlider();
			});
		}
	);
} else {
	var sprF = varFunctions(jQuery,jQuery.fn.owlCarousel);
	jQuery(document).ready(function(){
		sprF.initfunction();
		sprF.initSingleSlider();
		sprF.initSellSlider('.smv-best-sellers');
		sprF.initSellSlider('.smv-new-arrivals');
		sprF.initBrandSlider();
		sprF.handleToChangeTabContent();
		sprF.collapseNavBar();
		sprF.expandFilter();
	});
	jQuery(window).load(function(){
		
	});
	jQuery(window).on("resize",function(){
		sprF.initBrandSlider();
	});
}
function varFunctions($,owlCarousel) {
	
	var $spr = {
		initfunction: function () {
			$('.smv-product-list .smv-product-item').each(function(){
				if($(this).children().children().children('.smv-item-price').children('.smv-old-price').length  != 0){
					$(this).children().children().children('.smv-item-price').children('.smv-new-price').addClass('smv-red');
				}
			});
			
			$('.smv-head-right-bottom .smv-call-now').css('right', $('.smv-head-right').width() + 20);
		},
		initSingleSlider: function () {
			var slideCounter = 1;
			$(".smv-slide-banner .smv-slider").owlCarousel({
		      loop:true,
		      responsiveClass:true,
		      margin : 10,
		      items: slideCounter,
		      dots:true,
		      nav:  false
			});
		},
		handleToChangeTabContent: function(){
			$('.smv-btn-change-tab li a').on('click', function(e){
				e.preventDefault();
				var $self = $(this);
				var dataId = $self.data('id');
				var tabSeller = $('.smv-wrap-info .smv-tab-content');
				var selectorId = '.' + dataId;
				
				$('.smv-btn-change-tab li').removeClass('active');
				$self.parent().addClass('active');
				tabSeller.addClass('smv-hidden');
				$(selectorId).removeClass('smv-hidden');
			});
		},
		initSellSlider: function(selector) {
			if($(window).width() < 768){
				var slideCounter = $(window).width() > 639 ? 3 : $(window).width() > 480 ? 2 : 1;
				var owl_Carousel = $(selector).find('ul').owlCarousel({
					loop:true,
					responsiveClass:true,
					margin : 20,
					items: 3,
					dots:false,
					nav:  $(selector).find('ul li').length <= slideCounter ? false : true,
					navClass : ['owl-prev-product','owl-next-product'] ,
					navText : ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
					responsive:{
						0:{
						  items:1,
						  margin :10,
						},
						480:{
						  items:2,
						  margin :10,
						},
						640:{
						  items:3
						}
					}    
				});
				return owl_Carousel;
			}
		},
		initBrandSlider: function() {
			if($(window).width() < 640){
				$('.smv-our-brands ul.smv-brands-list').owlCarousel({
					loop:true,
					responsiveClass:true,
					margin : 10,
					items: 2,
					dots:false,
					nav:  $('.smv-our-brands ul.smv-brands-list li').length <= 2 ? false : true,
					navClass : ['owl-prev-brand','owl-next-brand'] ,
					navText : ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'] 
				});
			}else{
				//$('.smv-our-brands ul.smv-brands-list').data('owlCarousel').destroy();
				//$('.smv-our-brands ul.smv-brands-list').destroy.owl.carousel();
			}
		},
		collapseNavBar: function() {
			var width = $(window).width();
			if(width < 993){
				//Collapse Navigation on Mobile
				$('.smv-hide-des a').on('click', function(e){
					e.preventDefault();
					$('body').toggleClass('smv-collapse');
				});
				//Collapse Header Category
				$('.smv-category li a').off().on('click touchend', function(e){
					e.preventDefault();
					var $self = $(this).parent('li');
					
					if( $self.hasClass('active') ){
						$self.removeClass('active')
					} else {
						$self.addClass('active');
					}
				});
			}
		},
		expandFilter: function() {
			$('.smv-filter-group .smv-title').on('click', function(e){
				e.preventDefault();
				var $self = $(this).parent('.smv-filter-group');
				$(this).toggleClass('active');
				$self.children('.smv-filter-items').slideToggle();
			});

			$('.smv-filter-header').on('click', function(e){
				e.preventDefault();
				var $self = $(this).parent('.smv-widget');
				$(this).toggleClass('active');
				$self.children('.smv-filter-list').slideToggle();
			});

			$('.smv-mini-cart a').on('click touchend', function(e){
				e.preventDefault();
				var $self = $(this);
				var $parent = $self.closest('.smv-mini-cart');

				$('.smv-head-right .smv-mini-cart .smv-wrap-mini-cart').toggleClass('smv-show');

				$parent.toggleClass('active');
			});
		}
	}
	return $spr;
}